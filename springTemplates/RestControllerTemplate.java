package com.san;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.apms.rest.RESTRequest;
import com.apms.rest.RESTResponse;

@RestController
@RequestMapping("/home")
public class RestControllerTemplate {
	
    @Autowired
    private ServiceTService serviceTService;

    /*
    **Return a listing of all the resources
    */
    @GetMapping
	public RESTResponse<List<ServiceT>> getAll() {
		List<ServiceT> res;
		try {
			res = serviceTService.getAll();
		} catch (Exception e) {
			e.printStackTrace();
			return new RESTResponse<List<ServiceT>>(RESTResponse.DBFAIL, "Inconsistencia en la base de datos.", null);
		}
		if (!res.isEmpty()) {
			return new RESTResponse<List<ServiceT>>(RESTResponse.OK, "", res);
		} else {
			return new RESTResponse<List<ServiceT>>(RESTResponse.FAIL, "Los catalogos necesarios no se han cargado.", null);
		}
	}

    /*
    **Return one resource
    */
    @GetMapping("/{id}")
    public RESTResponse<ServiceT> getOne(@PathVariable Integer id) {
        ServiceT res;
        try{
            res = serviceTService.getOne(id);
        } catch (Exception e) {
            e.printStackTrace();
            return new RESTResponse<ServiceT>(RESTResponse.DBFAIL, "Inconsistencia en la base de datos.", null);
        }
        if (res != null) {
			return new RESTResponse<ServiceT>(RESTResponse.OK, "", res);
		} else {
			return new RESTResponse<ServiceT>(RESTResponse.FAIL, "ServiceT no registrado.", null);
		}
    }

    /*
    **Store a newly created resource in storage.
    */
    @PostMapping
    public RESTResponse<ServiceT> post(@RequestBody RESTRequest<ServiceT> serviceT) {
        try {
            if(serviceTService.getOne(serviceT.getPayload().getId()) != null)
                return new RESTResponse<ServiceT>(RESTResponse.FAIL, "ServiceT ya existe en el sistema.", null);
            serviceTService.add(serviceT.getPayload());
        } catch (Exception e) {
            e.printStackTrace();
            return new RESTResponse<ServiceT>(RESTResponse.FAIL, "Hubo un error en el registro. Por favor, intentelo mas tarde.", null);
        }
        return new RESTResponse<ServiceT>(RESTResponse.OK, "Registro finalizado exitosamente.", null);
    }

    /*
    **Update the specified resource in storage partially.
    */
    @PatchMapping
    public RESTResponse<ServiceT> patch(@RequestBody RESTRequest<ServiceT> serviceT) {
        try {
            serviceTService.update(serviceT.getPayload());
        } catch (Exception e) {
            e.printStackTrace();
            return new RESTResponse<ServiceT>(RESTResponse.FAIL, "Hubo un error al modificar. Por favor, intentelo mas tarde.", null);
        }
        return new RESTResponse<ServiceT>(RESTResponse.OK, "ServiceT modificado.", null);
    }

    /*
    **Update the specified resource in storage.
    */
    @PutMapping
    public RESTResponse<ServiceT> put(@RequestBody RESTRequest<ServiceT> serviceT) {
        try {
            serviceTService.update(serviceT.getPayload());
        } catch (Exception e) {
            e.printStackTrace();
            return new RESTResponse<ServiceT>(RESTResponse.FAIL, "Hubo un error al modificar. Por favor, intentelo mas tarde.", null);
        }
        return new RESTResponse<ServiceT>(RESTResponse.OK, "ServiceT modificado.", null);
    }

    /*
    **Remove the specified resource from storage.
    */
    @DeleteMapping("/{id}")
    public RESTResponse<ServiceT> delete(@PathVariable Integer id) {
        try {
            serviceTService.delete(id);
        } catch (Exception e) {
            e.printStackTrace();
            return new RESTResponse<ServiceT>(RESTResponse.FAIL, "Hubo un error en el registro. Por favor, intentelo mas tarde.", null);
        }
        return new RESTResponse<ServiceT>(RESTResponse.OK, "ServiceT modificado.", null);
    }
}
